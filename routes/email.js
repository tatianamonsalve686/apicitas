const nodemailer = require("nodemailer");

class Email{

    constructor(oConfig){
        this.createTransport= nodemailer.createTransport(oConfig);
    }
    enviarCorreo(oEmail){
        try {
            this.createTransport.sendMail(oEmail,function(error, info){
                if(error){
                  console.log("error al enviar el mail "+error );
                }
                else{
                    console.log("corrreo enviado exitosamente");
                }
            })
        } catch (error) {
            console.log("Email.enviarCorreo  --Error-- "+error);
        }
    }
}
module.exports = Email;