var express = require('express');
var router = express.Router();
const ObjectId = require('mongodb').ObjectId;
const email =require("./email")

// se configura la informacion del servidor de correo
const oEmail = new email({
  "host":"smtp.gmail.com",
  "port":"587",
  "secure":false,
  "auth":{
      "type":"login",
      "user":"tatianamonsalve686@gmail.com",
      "pass":"tvycyntvkntopbia"
  }

})
//implementacion servicio envio de correos
// metodo con el cual se van a enviar los correos
router.post('/mailer', (req, res, next) => {
  

  let email ={
      from:"contacto@custommadedesign.com",
      to:req.body.email,
      cc:"tatianamonsalve686@gmail.com",
      subject:"cita taller motos",
      html:`
          <div>
          <h3>Se ha agendado una cita correctamente</h3>
          <p>Nombre: ${req.body.name}</p>
          <p>Telefono: ${req.body.phone}</p>
          <p>Correo: ${req.body.email}</p>
          <p>Fecha: ${req.body.appointmentDate}</p>
          <p>Hora: ${req.body.hour}</p>
          </div>
      `
  }
  //se ejecuta el envio del correo
  oEmail.enviarCorreo(email);
  res.send("ok")
});

//CONSULTAR TODAS LAS CITAS
router.get('/appointments', (req, res, next) => {
  req.collectionAppoinments.find({})
    .toArray()
    .then(results => res.json(results))
    .catch(error => res.send(error));
});


//SERVICIO PARA INICIAR SESION DEL TECNICO
router.post('/login', async(req, res, next) => {
  //se obtiene usuario y clave desde el body
   const { user, pass} = req.body;

   //se obtiene la info del usuario en la bd
  const dbUser=await req.collectionUsers.findOne({user});
//se valida que el usuario exista y que la informacion coincida
  if(!dbUser || dbUser.pass!==pass)return res.status(401).send("Datos errados")
//si la informacion es correcta entonces confirma la autenticacion

  return res.status(200).json({message:'Autenticacion correcta'})  
});
//CREAR USUARIO
router.post('/create-users', (req, res, next) => {
  // SE OBTIENE USUARIO Y CLAVE DESDE EL BODY
  const { user, pass} = req.body;
  //SE VALIDA QUE EXISTA USUARIO Y CLAVE EN EL BODY
  if (!user || !pass ) {
    return res.status(400).json({
      message: 'faltan datos para crear el usuario',
    });
  }
//SE ENVIA LA INFORMACIÓN A LA BASE DE DATOS
  const payload = { user, pass};
  req.collectionUsers.insertOne(payload)
    .then(result => res.json(payload))
    .catch(error => res.send(error));
});
//CREAR CITAS
router.post('/appointments', (req, res, next) => {
  // se obtiene la informacion de la cita desde el body
  const { cc, appointmentDate, name, email, hour, phone, service } = req.body;
  //se valida que la informacion de la cita este completa
  if (!cc || !appointmentDate || !name || !email || !hour || !phone || !service) {
    return res.status(400).json({
      message: 'Es necesario diligenciar todos los campos para la cita',
    });
  }
// se llama el metodo para insertar en la bd enviandole la informacion de la cita
  req.collectionAppoinments.insertOne(req.body)
    .then(result => res.json(req.body))
    .catch(error => res.send(error));
});

//FILTRAR CITA POR CEDULA
router.post('/find-users-appointments', (req, res, next) => {
  //se obtiene cc del body
  const { cc } = req.body;
//se valida que llega informacion en la cc
  if (!cc ) {
    return res.status(400).json({
      message: 'Es necesario diligenciar el documento para filtrar la cita',
    });
  }
/*se guarda la consulta en query */
  var query = { cc: cc };
  //se consulta la cita a la bd enviando la cc
  req.collectionAppoinments.find(query)
  .toArray()
  .then(results => res.json(results))
  .catch(error => res.statusCode(401).send(error));
});

//FILTRAR POR FECHA
router.post('/filter-appointments', (req, res, next) => {
  // se define una constante para obtener la fecha del body
  const { appointmentDate } = req.body;

  // aqui se valida que la información de la fecha exista
  if (!appointmentDate ) {
    return res.status(400).json({
      message: 'Es necesario diligenciar la fecha para filtrar la cita',
    });
  }

  /* se guarda la consulta en query */
  var query = { appointmentDate: appointmentDate };
  //se reliza la consulta de citas enviando la fecha 
  req.collectionAppoinments.find(query)

  .toArray()
  .then(results => res.json(results))
  .catch(error => res.send(error));
});

// a difierencia de los otros servicios
// este recibe un parametro en la url
router.delete('/appointments/:id', (req, res, next) => {
// a continuacion  se guarda en una constante el parametro id
  const { id } = req.params;
  /*el id se guarda como un dato de  objectId de mongo*/
  const _id = ObjectId(id)

  req.collectionAppoinments.deleteOne({ _id })

    .then(result => res.json(result))
    .catch(error => res.send(error));
});

module.exports = router;